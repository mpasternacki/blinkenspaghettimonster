mod colorgen;
mod config;
mod coordgen;

use std::path::PathBuf;

use anyhow::Result;
use clap::Parser;

fn elaborate_input_path() -> PathBuf {
    use std::env;

    let mut path = env::var_os("CARGO_MANIFEST_DIR")
        .map(PathBuf::from)
        .unwrap_or(env::current_dir().unwrap());
    path.pop();
    path.push("spaghetti/config.ron");
    path
}

#[derive(Debug, Parser)]
struct Args {
    /// Generate colors
    #[arg(long, default_value_t = false)]
    colors: bool,

    /// Generate coords
    #[arg(long, default_value_t = false)]
    no_coords: bool,

    /// Coords file to load
    #[arg(long, short, default_value_os_t=elaborate_input_path())]
    file: PathBuf,
}

fn main() -> Result<()> {
    env_logger::init();

    let args = dbg!(Args::parse());
    let config = config::Config::load(&args.file)?;

    let coords_rs = coordgen::generate(&config)?;

    let colors_rs = colorgen::generate()?;

    let mut inputs: Vec<bat::Input> = Vec::new();

    if !args.no_coords {
        inputs.push(bat::Input::from_bytes(coords_rs.as_bytes()).name("coords.rs"));
    }

    if args.colors {
        inputs.push(bat::Input::from_bytes(colors_rs.as_bytes()).name("colors.rs"));
    }

    bat::PrettyPrinter::new()
        .header(true)
        .grid(true)
        .line_numbers(true)
        .language("rust")
        .paging_mode(bat::PagingMode::QuitIfOneScreen)
        .inputs(inputs)
        .print()?;

    Ok(())
}

use cichlid::ColorRGB;

use cortex_m::asm::delay;
use cortex_m::peripheral::{syst::SystClkSource, SYST};
use embedded_hal::digital::v2::OutputPin;

pub const CYCLE_TICKS: u32 = 30;

// T0H 19.2; 0.4μs
// T0L 40.8; 0.85μs
pub const T0H_TICKS: u32 = 10;

// T1H 38.4; 0.8μs
// T1L 21.6; 0.45μs
pub const T1H_TICKS: u32 = 23;

// RESET: >50μs

pub struct WS2812B<'a, T>
where
    T: OutputPin,
{
    pin: &'a mut T,
    syst: &'a mut SYST,
}

impl<'a, T> WS2812B<'a, T>
where
    T: OutputPin,
{
    pub fn new(pin: &'a mut T, syst: &'a mut SYST) -> WS2812B<'a, T> {
        syst.set_clock_source(SystClkSource::Core);
        syst.set_reload(CYCLE_TICKS - 1);
        syst.clear_current();
        syst.enable_counter();
        WS2812B { pin, syst }
    }

    #[inline(always)]
    #[allow(unused_must_use)]
    fn send_(&mut self, h_ticks: u32) {
        // if self.syst.has_wrapped() {
        //     panic!()
        // }
        while !self.syst.has_wrapped() {}
        cortex_m::interrupt::free(|_| {
            self.pin.set_high();
            delay(h_ticks);
            self.pin.set_low();
        });
    }

    #[inline]
    pub fn send_byte(&mut self, byte: u8) {
        // if self.syst.has_wrapped() {
        //     panic!()
        // }
        let mut mask: u8 = 0x80;
        while mask > 0 {
            self.send_(if byte & mask == 0 {
                T0H_TICKS
            } else {
                T1H_TICKS
            });
            mask >>= 1;
        }
    }

    pub fn send(&mut self, pixels: &[ColorRGB]) {
        self.start();
        for rgb in pixels {
            self.send_byte(rgb.g);
            self.send_byte(rgb.r);
            self.send_byte(rgb.b);
        }
    }

    #[inline]
    pub fn start(&mut self) {
        while !self.syst.has_wrapped() {
            core::hint::spin_loop();
        }
    }
}

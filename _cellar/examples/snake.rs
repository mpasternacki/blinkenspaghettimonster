#![feature(never_type)]
#![no_std]
#![no_main]
#![allow(dead_code)] // FIXME

extern crate cichlid;
extern crate embedded_hal;

use cortex_m_rt::{entry, pre_init};

use teensy_lc;
use teensy_lc::prelude::*;

use blinkenspaghettimonster::ws2812b::WS2812B;

// C5 is LED
// B1 is 17 / 5V out

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

const N_LEDS: usize = 600;
const SNAKE_LEN: i16 = 256;

#[entry]
fn main() -> ! {
    let pp = teensy_lc::dev::Peripherals::take().unwrap();
    let cpp = teensy_lc::dev::CorePeripherals::take().unwrap();

    //// Turn LED on
    // Port C Clock Gate Control: enable clock to port B & C
    pp.SIM
        .scgc5
        .write(|w| w.portc().set_bit().portb().set_bit());

    let fgpioc = pp.FGPIOC.split();
    fgpioc.pc5.enable();
    let mut led_pin = fgpioc.pc5.into_output();
    led_pin.set_high().unwrap();

    let fgpiob = pp.FGPIOB.split();
    fgpiob.pb1.enable();
    // Enable pulldown on B1
    pp.PORTB.pcr1.modify(|_, w| w.pe().set_bit().ps()._0());
    let mut ws2812b_pin = fgpiob.pb1.into_output();
    ws2812b_pin.set_low().unwrap();
    teensy_lc::delay_usec(1_000_000);
    led_pin.set_low().unwrap();

    let mut syst = cpp.SYST;
    let mut ws2812b = WS2812B::new(&mut ws2812b_pin, &mut syst);

    // rainbow!
    let mut color = cichlid::HSV::new(0, 255, 255);
    let mut frame: usize = 0;
    let mut start: i16 = -SNAKE_LEN;
    let mut snake: [cichlid::ColorRGB; N_LEDS as usize] =
        [cichlid::ColorRGB::Black; N_LEDS as usize];
    loop {
        // precalculate the snake
        for i in 0..N_LEDS {
            let pos = i as i16 - start;
            if pos < 0 || pos > SNAKE_LEN {
                // black outside of the snake
                snake[i] = cichlid::ColorRGB::Black;
                continue;
            }
            let unscaled_v: i16 = if pos <= SNAKE_LEN / 2 {
                pos
            } else {
                SNAKE_LEN - pos
            };
            color.v = (unscaled_v << 8 / (SNAKE_LEN / 2)) as u8;
            color.h = (255 - (i << 2) + frame) as u8;
            snake[i] = color.to_rgb_rainbow();
        }

        // send it
        ws2812b.send(&snake);

        // prepare next frame
        frame += 1;
        if frame & 0x001f == 0 {
            led_pin.toggle();
        }
        start += 1;
        if start > N_LEDS as i16 + SNAKE_LEN {
            start = -SNAKE_LEN;
        }

        // pause between frames
        teensy_lc::delay_usec(10_000);
    }
}

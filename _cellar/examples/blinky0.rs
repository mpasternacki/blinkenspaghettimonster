#![no_std]
#![no_main]
#![feature(asm)]

use cortex_m;
use cortex_m_rt::{entry, pre_init};

use blinkenspaghettimonster::teensy_lc::{self, dev};

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

#[entry]
fn main() -> ! {
    let pp = dev::Peripherals::take().unwrap();
    // let cpp = dev::CorePeripherals::take().unwrap();

    //// Initialize LED to on
    // Port C Clock Gate Control: enable clock to port C
    pp.SIM.scgc5.write(|w| w.portc().set_bit());

    // Port C pin control register #5, bits 10-8 001: set port C to GPIO
    pp.PORTC.pcr5.write(|w| w.mux()._001());

    // Fast GPIO port C / port data direction register; bit 5 out
    pp.FGPIOC.pddr.write(|w| unsafe { w.bits(1 << 5) });

    cortex_m::interrupt::disable();
    loop {
        pp.FGPIOC.psor.write(|w| unsafe { w.bits(1 << 5) });
        teensy_lc::delay_usec(100_000);
        pp.FGPIOC.pcor.write(|w| unsafe { w.bits(1 << 5) });
        teensy_lc::delay_usec(900_000);
    }
}

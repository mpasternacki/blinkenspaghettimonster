#![no_std]
#![no_main]

extern crate cichlid;

use cortex_m_rt::{entry, pre_init};

use blinkenspaghettimonster::teensy_lc;
use blinkenspaghettimonster::teensy_lc::prelude::*;
use blinkenspaghettimonster::ws2812b::WS2812B;

const BRIGHTNESS: u8 = 128;
const N_LEDS: u16 = 600;

// C5 is LED
// B1 is 17 / 5V out

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

#[entry]
fn main() -> ! {
    let pp = teensy_lc::dev::Peripherals::take().unwrap();
    let cpp = teensy_lc::dev::CorePeripherals::take().unwrap();

    //// Turn LED on
    // Port C Clock Gate Control: enable clock to port B & C
    pp.SIM
        .scgc5
        .write(|w| w.portc().set_bit().portb().set_bit());

    let fgpioc = pp.FGPIOC.split();
    fgpioc.pc5.enable();
    let mut led_pin = fgpioc.pc5.into_output();
    led_pin.set_high().unwrap();

    let fgpiob = pp.FGPIOB.split();
    fgpiob.pb1.enable();
    // Enable pulldown on B1
    pp.PORTB.pcr1.modify(|_, w| w.pe().set_bit().ps()._0());
    let mut ws2812b_pin = fgpiob.pb1.into_output();
    ws2812b_pin.set_low().unwrap();
    teensy_lc::delay_usec(1_000_000);

    let mut syst = cpp.SYST;
    let mut ws2812b = WS2812B::new(&mut ws2812b_pin, &mut syst);

    cortex_m::interrupt::free(|_| {
        for _ in 0..(N_LEDS * 3) {
            ws2812b.send_byte(BRIGHTNESS);
        }
    });
    loop {}
}

#!/usr/bin/env python3
import math

N = 50
RADIUS = 0.249
ANGLE = math.pi * 0.42

ANGLE_STEP = (2*ANGLE)/(N-1)

def gen_circle(center_x: float):
    for i in range(N):
        angle = -ANGLE + i * ANGLE_STEP
        yield (math.sin(angle)*RADIUS + center_x, 0.5 - math.cos(angle)*RADIUS)


print(repr([*gen_circle(RADIUS), *gen_circle(1-RADIUS)]))

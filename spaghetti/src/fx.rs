use core::f32::consts::TAU;

use rand::prelude::*;

use crate::show::{Duration, Show};

mod _prelude;
mod _util;
pub mod bounce;
pub mod crawl;
pub mod fire;
pub mod fireworks;
pub mod flood;
pub mod plasma;
pub mod snek;
pub mod transition;
pub mod zoomies;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Fx {
    // Effects
    Fire,
    Fireworks,
    Plasma,
    Snek,
    Crawl,
    Bounce,
    Zoomies,

    // Transitions?
    Flood(usize),
    // FloodWave(flood::Direction),
}

impl Distribution<Fx> for rand::distributions::Standard {
    #[cfg(not(feature = "chain"))]
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Fx {
        match rng.gen_range(0..=7) {
            0 => Fx::Fire,
            1 => Fx::Fireworks,
            2 => Fx::Plasma,
            3 => Fx::Snek,
            4 => Fx::Crawl,
            5 => Fx::Flood(rng.gen_range(0..360)),
            // 6 => Fx::FloodWave(rng.gen()),
            6 => Fx::Bounce,
            7 => Fx::Zoomies,
            _ => unreachable!(),
        }
    }

    #[cfg(feature = "chain")]
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Fx {
        match rng.gen_range(0..=5) {
            0 => Fx::Fire,
            1 => Fx::Fireworks,
            2 => Fx::Plasma,
            3 => Fx::Snek,
            4 => Fx::Crawl,
            5 => Fx::Flood(rng.gen_range(0..360)),
            _ => unreachable!(),
        }
    }
}

impl Fx {
    pub async fn play<SH: Show>(&self, sh: &mut SH) {
        log::info!("Playing {}", self);
        match self {
            Fx::Fire => {
                let fire_duration = Duration::secs(sh.rng().gen_range(17..=23));
                fire::fire(&mut sh.with_timeout_hint(fire_duration)).await;
            }
            Fx::Fireworks => {
                fireworks::fireworks(sh).await;
            }
            Fx::Plasma => {
                plasma::plasma(&mut sh.with_timeout_hint(Duration::minutes(1))).await;
            }
            Fx::Snek => {
                // Hard timeout in case it gets stuck
                SH::timeout(Duration::minutes(6), snek::snek(sh)).await;
            }
            Fx::Crawl => {
                crawl::crawl(sh).await;
            }
            Fx::Flood(dir) => {
                let dir_rad = *dir as f32 * TAU / 360.;
                flood::flood(dir_rad, sh).await;
            }
            // Fx::FloodWave(dir) => {
            //     flood::wave(*dir, sh).await;
            // }
            Fx::Bounce => {
                bounce::bounce(sh).await;
            }
            Fx::Zoomies => {
                zoomies::zoomies(&mut sh.with_timeout_hint(Duration::secs(15))).await;
            }
        }
        transition::fade(0.1, sh).await;
    }

    pub const fn all() -> &'static [Self] {
        &[
            Self::Fire,
            Self::Fireworks,
            Self::Plasma,
            Self::Snek,
            Self::Crawl,
            Self::Bounce,
            Self::Zoomies,
        ]
    }
}

impl core::fmt::Display for Fx {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        core::fmt::Debug::fmt(self, f)
    }
}

pub async fn main<SH: Show>(sh: &mut SH) -> ! {
    loop {
        // Fx::play_seq(sh).await;
        sh.rng().gen::<Fx>().play(sh).await;
    }
}

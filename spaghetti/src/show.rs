use ::core::pin::Pin;
use core::{
    future::Future,
    ops::{Deref, DerefMut},
    task::Poll,
};

use alloc::{boxed::Box, string::String};

use async_trait::async_trait;
use palette::Srgb;

use crate::coords::Coords;

#[derive(Clone, Copy)]
pub struct Frame([Srgb; Coords::N]);

impl Deref for Frame {
    type Target = [Srgb; Coords::N];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Frame {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Default for Frame {
    fn default() -> Self {
        Self::new()
    }
}

impl Frame {
    pub const N: usize = Coords::N;
    pub const OFF: Srgb = crate::colors::BLACK;

    pub const fn new() -> Self {
        Self([Self::OFF; Self::N])
    }

    pub fn clear(&mut self) {
        self.fill(Self::OFF);
    }
}

pub const FPS: fugit::HertzU32 = fugit::HertzU32::from_raw(30);
pub type Duration = fugit::Duration<u32, 1, { FPS.raw() }>;

#[derive(Copy, Clone, Debug)]
pub enum Hint {
    /// Default, continue animation
    PleaseContinue,

    /// Hint that animation should prettily finish
    PleaseFinish,
}

impl Default for Hint {
    fn default() -> Self {
        Hint::PleaseContinue
    }
}

impl Hint {
    pub fn should_finish(&self) -> bool {
        match self {
            Hint::PleaseContinue => false,
            Hint::PleaseFinish => true,
        }
    }
}

#[async_trait]
pub trait Show: Send {
    type Rng: rand::Rng + Send + Sync;

    async fn frame_done(&mut self) -> Hint;
    fn frame_mut(&mut self) -> &mut Frame;
    fn frame(&self) -> &Frame;
    fn rng(&mut self) -> &mut Self::Rng;
    fn set_status(&mut self, status: String);
    async fn delay(duration: Duration);

    async fn timeout(duration: Duration, inner: impl Future + Send) {
        futures::future::select(core::pin::pin!(inner), Self::delay(duration)).await;
    }

    fn with_future_hint(
        &mut self,
        future: Pin<Box<dyn Future<Output = Hint> + Send>>,
    ) -> ShowWithHint<'_, Self>
    where
        Self: Sized,
    {
        ShowWithHint::new(self, future)
    }

    fn with_timeout_hint<'s>(&'s mut self, timeout: Duration) -> ShowWithHint<'_, Self>
    where
        Self: Sized,
    {
        self.with_future_hint(Box::pin(async move {
            Self::delay(timeout).await;
            Hint::PleaseFinish
        }))
    }
}

pub struct ShowWithHint<'a, SH: Show> {
    sh: &'a mut SH,
    ft: Pin<Box<dyn Future<Output = Hint> + Send>>,
    hint: Option<Hint>,
}

impl<'a, SH: Show> ShowWithHint<'a, SH> {
    pub fn new(sh: &'a mut SH, ft: Pin<Box<dyn Future<Output = Hint> + Send>>) -> Self {
        Self { sh, ft, hint: None }
    }
}

#[async_trait]
impl<'a, SH: Show> Show for ShowWithHint<'a, SH> {
    type Rng = SH::Rng;

    async fn frame_done(&mut self) -> Hint {
        match self.sh.frame_done().await {
            Hint::PleaseContinue => match self.hint {
                Some(hint) => hint,
                None => match futures::poll!(&mut self.ft) {
                    Poll::Pending => Hint::PleaseContinue,
                    Poll::Ready(hint) => {
                        self.hint = Some(hint);
                        hint
                    }
                },
            },
            other => other,
        }
    }

    fn frame_mut(&mut self) -> &mut Frame {
        self.sh.frame_mut()
    }

    fn frame(&self) -> &Frame {
        self.sh.frame()
    }

    fn rng(&mut self) -> &mut Self::Rng {
        self.sh.rng()
    }

    fn set_status(&mut self, status: String) {
        self.sh.set_status(status);
    }

    // No async, just delegate with the same type, to avoid re-boxing the response
    #[must_use]
    fn delay<'b>(duration: Duration) -> Pin<Box<dyn Future<Output = ()> + Send + 'b>> {
        SH::delay(duration)
    }
}

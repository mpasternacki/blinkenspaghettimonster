use crate::prelude::*;

use palette::{IntoColor, Srgb};

type Duration = <Systick as rtic_monotonics::Monotonic>::Duration;

const COLORS: usize = 8;
const STRIDE: usize = 4;
const FULL_LENGTH: usize = COLORS * STRIDE;
const LEAPS: usize = (Coords::N + FULL_LENGTH - 1) / FULL_LENGTH;
const D_ON: Duration = Duration::millis(100 * 5);
const D_OFF: Duration = Duration::millis(50 * 5);
const D_WAIT: Duration = Duration::secs(5); // TODO: trigger?

fn colors() -> [Srgb; COLORS] {
    let mut colors: [Srgb; COLORS] = [spaghetti::colors::ALICEBLUE * 0.2; 8];
    for (i, color) in colors.iter_mut().enumerate() {
        let hue = (i as f32 + 0.5) / COLORS as f32 * 360.;
        *color = palette::Hsv::new(hue, 1., 1.).into_color();
    }
    colors
}

#[allow(clippy::needless_pass_by_ref_mut)] // the mut is actually needed
pub(crate) async fn calibrate(sh: &mut impl spaghetti::Show) {
    let colors = colors();

    sh.frame_mut().clear();
    sh.frame_done().await;

    loop {
        Systick::delay(D_WAIT).await;
        log::info!("STARTING CALIBRATION PATTERN {LEAPS} leaps");

        let mut visited: [bool; Coords::N] = [false; Coords::N];
        for step in 0..STRIDE {
            for leap in 0..LEAPS {
                let start = leap * FULL_LENGTH + step;
                log::info!(
                    "Frame #{}/{}, {leap}*{FULL_LENGTH}+{step}={start}",
                    step * LEAPS + leap,
                    STRIDE * LEAPS,
                );

                for (j, &color) in colors.iter().enumerate() {
                    let idx = start + j * STRIDE;
                    if idx < sh.frame().len() {
                        sh.frame_mut()[idx] = color;
                        visited[idx] = true;
                    }
                }
                sh.frame_done().await;
                Systick::delay(D_ON).await;

                sh.frame_mut().clear();
                sh.frame_done().await;
                Systick::delay(D_OFF).await;
            }
        }

        let mut n_unvisited: usize = 0;
        for unvisited in
            visited
                .into_iter()
                .enumerate()
                .filter_map(|(i, visited)| if visited { None } else { Some(i) })
        {
            log::warn!("Unvisited LED: {}", unvisited);
            n_unvisited += 1;
        }

        log::info!("FINISHED CALIBRATION PATTERN ({n_unvisited} unvisited leds)");
    }
}

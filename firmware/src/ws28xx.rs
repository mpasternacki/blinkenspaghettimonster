use bsp::{hal, pins::imxrt_iomuxc as iomuxc, ral};

mod dma_destination;
mod flexio;

#[derive(Clone, Copy, Debug)]
pub enum SendError {
    DMAChannelStillActive,
    FlexIOShifterStillActive,
    DMAError(hal::dma::Error),
}

pub struct Ws28xx<const N: u8, const NP: u8, P>
where
    ral::flexio::Instance<N>: ral::Valid,
    P: iomuxc::flexio::Pin<NP>,
{
    flexio: ral::flexio::Instance<N>,
    _pin: P,
    shifter_id: u8,
    dma_signal: u32,
    channel: hal::dma::channel::Channel,
}

impl<const N: u8, const NP: u8, P> Ws28xx<N, NP, P>
where
    ral::flexio::Instance<N>: ral::Valid,
    P: iomuxc::flexio::Pin<NP>,
{
    pub fn prepare(
        flexio: ral::flexio::Instance<N>,
        mut pin: P,
        mut channel: hal::dma::channel::Channel,
        dma_signal: u32,
    ) -> Self {
        // Set output pin to FlexIO
        iomuxc::flexio::prepare(&mut pin);
        let cfg = flexio::FlexIOConfig::for_pin(&pin);
        let shifter_id = cfg.shifter;

        crate::flexio::reset(&flexio);
        cfg.apply(&flexio);
        crate::flexio::enable(&flexio);

        channel.set_interrupt_on_completion(true);

        Self {
            flexio,
            _pin: pin,
            shifter_id,
            channel,
            dma_signal,
        }
    }

    fn is_shifter_active(&self) -> bool {
        (ral::read_reg!(ral::flexio, self.flexio, SHIFTSTAT) & (1 << self.shifter_id)) == 0
    }

    /// Send buffer to LEDs. Buffer should be already transmuted to an
    /// u32 array, which is left as an exercise to the reader (the
    /// reader is myself).
    ///
    /// At the end of the buffer there should be some zeroes,
    /// otherwise output will be stuck at 1 because I have no idea
    /// why. I'm not sure how much zeroes is needed, maybe just the
    /// last bit, maybe one byte, maybe entire word. The framebuffer
    /// I'm using here leaves the entire word.
    pub async fn send(
        &mut self,
        buffer: &[u32],
        mut delay: impl embedded_hal_async::delay::DelayUs,
    ) -> Result<(), SendError> {
        if self.channel.is_active() {
            return Err(SendError::DMAChannelStillActive);
        }
        if self.is_shifter_active() {
            return Err(SendError::FlexIOShifterStillActive);
        }

        // Now sleep for 1ms to ensure the reset signal was sent
        delay.delay_ms(1).await;

        let mut dest = dma_destination::Ws28xxDestination::new(
            &self.flexio,
            self.dma_signal,
            self.shifter_id.into(),
        );

        hal::dma::peripheral::write(&mut self.channel, buffer, &mut dest)
            .await
            .map_err(SendError::DMAError)?;

        Ok(())
    }
}

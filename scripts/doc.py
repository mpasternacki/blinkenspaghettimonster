#!/usr/bin/env python3

from pathlib import Path
import argparse
import collections
import itertools
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--no-generate", action="store_true")
parser.add_argument("-o", "--open", action="store_true")
args = parser.parse_args()

ROOT = Path(__file__).parent.parent.absolute()

if not args.no_generate:
    for cargo_toml in ROOT.glob("*/Cargo.toml"):
        print(f"\n*** Documenting {cargo_toml.parent.name}\n")
        subprocess.run(["cargo", "doc"], cwd=cargo_toml.parent, check=True)

DOCS = collections.defaultdict(dict)

TARGET = ROOT / "target"
for index_html in itertools.chain(
    TARGET.glob("doc/*/index.html"), TARGET.glob("*/doc/*/index.html")
):
    index_html = index_html.relative_to(TARGET)
    crate = index_html.parent.name
    target = index_html.parent.parent.parent.name or None
    DOCS[target][crate] = index_html

OUT = TARGET.joinpath("index.html")
with OUT.open("w") as index_html:
    print(
        """\
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Doc Index</title>
  </head>
  <body>
  <h1>Doc Index</h1>""",
        file=index_html,
    )
    for target in sorted(DOCS.keys(), key=lambda k: k or ""):
        if target is not None:
            print(f"<h2>{target}</h2>", file=index_html)
        print("<ul>", file=index_html)
        for crate in sorted(DOCS[target].keys()):
            crate_index_html = DOCS[target][crate]
            print(f"<li><a href='{crate_index_html}'>{crate}</a></li>", file=index_html)
        print("</ul>", file=index_html)
    print(
        """\
  </body>
</html>""",
        file=index_html,
    )

if args.open:
    subprocess.run(["xdg-open", str(OUT)], check=True)
